package sortersTesterClasses;

public class Entero implements Comparable{ 
	private int value; 

	public Entero(int v) { 
		value = v;
	}

	public int getValue() {
		return value; 
	} 

	public String toString() { 
		return value + ""; 
	}

	@Override
	public int compareTo(Object arg0) {
		
		if(arg0 == null) {
			throw new NullPointerException("Specified Object is null");
		}
		
		Entero obj;
		try {
			obj = (Entero) arg0;
		} catch (Exception e) {
			throw new ClassCastException("specified object must be of type Entero");
		}
		
		return this.getValue() - obj.getValue();
	}

 } 