package sortersTesterClasses;

import java.util.ArrayList;
import java.util.Comparator;

import interfaces.Sorter;
import sorterClasses.BubbleSortSorter;
import sorterClasses.CocktailSortSorter;
import sorterClasses.CombSortSorter;
import sorterClasses.InsertionSortSorter;
import sorterClasses.SelectionSortSorter;

public class MyTester2 {
	
	private static ArrayList<Sorter<Integer>> sortersList = new ArrayList<>(); 

	private static Integer[] arr = {5, 9, 20, 22, 20, 5, 4, 13, 17, 8, 22, 1, 3, 7, 11, 9, 10};

	public static void main(String[] args) {

		sortersList.add(new BubbleSortSorter<Integer>()); 
		sortersList.add(new SelectionSortSorter<Integer>()); 
		sortersList.add(new InsertionSortSorter<Integer>()); 
		sortersList.add(new CombSortSorter<Integer>()); 
		sortersList.add(new CocktailSortSorter<Integer>()); 
		
		System.out.println("Original Array: ");
		printArr(arr);
		
		for(Sorter<Integer> s : sortersList){
			s.sort(arr, null);
			printArr(arr, s, null);
			
			s.sort(arr, new IntegerComparator1());
			printArr(arr, s, new IntegerComparator1());
			
			s.sort(arr, new IntegerComparator2());
			printArr(arr, s, new IntegerComparator2());
		}
		
	}

	private static void printArr(Integer[] arr2, Sorter<Integer> s, Comparator<Integer> cmp) {
		String c;
		
		if(cmp == null){
			c = "null";
		}
		else if(cmp instanceof IntegerComparator1)
			c = "Forwards comparator";
		else
			c = "Backwards comparator";
		
		System.out.print(s.getName() +", "+ c + ", Integer Array: "); 
		printArr(arr);
		
	}
	
	private static void printArr(Integer[] arr){ 
		for (int i=0; i<arr.length; i++) 
			System.out.print("\t" + arr[i]); 
		
		System.out.println();
	}

}
