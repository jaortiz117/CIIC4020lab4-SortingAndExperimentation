package sortersTesterClasses;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

import interfaces.Sorter;
import sorterClasses.BubbleSortSorter;
import sorterClasses.InsertionSortSorter;
import sorterClasses.SelectionSortSorter;

public class MyTester {

	private static Sorter<Entero> sorter;
	
	private static Random rnd = new Random(101); 

	private static ArrayList<Sorter<Entero>> sortersList = new ArrayList<>(); 
	
	private static Entero[] arr = new Entero[10];

	public static void main(String[] args) { 
		sortersList.add(new BubbleSortSorter<Entero>()); 
		sortersList.add(new SelectionSortSorter<Entero>()); 
		sortersList.add(new InsertionSortSorter<Entero>()); 
		
		for(int i=0; i< arr.length; i++){
			arr[i] = new Entero(rnd.nextInt(100));
		}
		
		for(Sorter<Entero> s : sortersList){
			s.sort(arr, null);
			
			printArr(arr, s);
		}
		
	}

	private static void printArr(Entero[] arr, Sorter<Entero> s) {

		System.out.print(s.getName() + " Entero Array: "); 
		for (int i=0; i<arr.length; i++) 
			System.out.print("\t" + arr[i]); 
		
		System.out.println();
		
	}

}
