package sorterClasses;

public class CombSortSorter<E> extends AbstractSorter<E>{

	public CombSortSorter() {
		super("Comb Sort");
	}

	@Override
	protected void auxSort() {

	      boolean proceed = true;
	      for(int i = 0; i < arr.length && proceed; i++) {
	         proceed = false;
	         for(int j = 0; j < arr.length - i - 1; j++) {
	            if(cmp.compare(arr[j], arr[j+1]) > 0) {	               
	               super.swapArrayElements(j, j+1);
	               proceed = true;
	            }
	         }
	      }
		
	}

}
